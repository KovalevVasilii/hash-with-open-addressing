#include <iostream>
#include <string>
#include <fstream>
#include <ctime>
using namespace std;
class NODE
{
private:
	string key;
	string value;
public:
	NODE(string key, string value)
	{
		this->key = key;
		this->value = value;
	}
	 
	string get_key()
	{
		return key;
	}

	string get_value()
	{
		return value;
	}

	void set_value(string value)
	{
		this->value = value;
	}
};


int coll = 0;
int num = 0;
class HASH_MAP
{
private:
	NODE **table;
	int table_size;
public:
	HASH_MAP(int TABLE_SIZE_)
	{
		table_size = TABLE_SIZE_;
		table = new NODE*[table_size];
		for (int i = 0; i < table_size; i++)
		{
			table[i] = nullptr;
		}
	}

	string get(string key) 
	{
		int loop = 0;
		for (int i = 0; i < key.length(); i++)
		{
			loop = loop + int(key[i]);
		}
		//cout << loop << endl;
		int hash = (loop % table_size);
		while (table[hash] != nullptr && table[hash]->get_key() != key) 
		{
			hash = (hash + int(key[0]) + int(key[1])) % table_size;
		}
		if (table[hash] == nullptr)
			return "-1";
		else
			return table[hash]->get_value();
	}

	void put(string key, string value)
	{
		num++;
		int loop = 0;
		for (int i = 0; i < key.length(); i++)
		{
			loop = loop + int(key[i]);
		}

		//cout << loop << endl;
		int hash = (loop % table_size);		
		while (true)
		{

			if (table[hash] == nullptr)
			{
				table[hash] = new NODE(key, value);
				break;
			}
			else
			{
				coll++;
				hash = (hash + int(key[0]) + int(key[1])) % table_size;
			}
		}
	}
};
int main()
{
	char buff[1500];
	HASH_MAP Hash(1000);
	ifstream fin("test.txt");
	int it = 0;
	if (!fin.is_open())
		cout << "Can not open\n";
	else
	{
		while (fin.getline(buff, 1500))
		{

			Hash.put(buff, buff);
		}

		fin.close();
	}


	cout << "Collisions: " << coll << endl;

	time_t end = 0, start = time(0);
	cout << "Up : " << Hash.get("Up") << endl;
	end = time(0) - start;
	cout << "Time to search: " << end << endl;
	cout << "Number of hash's: " << num << endl;
	cout << "Desert : " << Hash.get("Desert") << endl;
	cin.get();
	return 0;
}